package {
	import flash.display.*;
	public class Movable extends Sprite {
		public var velocityx:Number = 0;
		public var velocityy:Number = 0;
		public var accelx:Number = 0;
		public var accely:Number = 0;

		public var maxvelocityx:Number = -1;
		public var maxvelocityy:Number = -1;

		public var deceleration:Number = -1;

		public var minx:Number = Number.NEGATIVE_INFINITY;
		public var maxx:Number = Number.POSITIVE_INFINITY;
		public var miny:Number = Number.NEGATIVE_INFINITY;
		public var maxy:Number = Number.POSITIVE_INFINITY;

		public function update(elapsed:Number):void {
			updatePosition(elapsed);
		}

		public function updatePosition(elapsed:Number):void {
			velocityx += (accelx * elapsed);
			velocityy += (accely * elapsed);
			if(deceleration > 0) {
				velocityx /= (1 + (deceleration * elapsed));
				velocityy /= (1 + (deceleration * elapsed));
			}
			if(maxvelocityx >= 0) {
				if(velocityx >= maxvelocityx) velocityx = maxvelocityx;
				if(velocityx <= -maxvelocityx) velocityx = -maxvelocityx;
			}
			if(maxvelocityy >= 0) {
				if(velocityy >= maxvelocityy) velocityy = maxvelocityy;
				if(velocityy <= -maxvelocityy) velocityy = -maxvelocityy;
			}
			x += (velocityx * elapsed);
			y += (velocityy * elapsed);
			if(x < minx) {
				if(accelx < 0) accelx = 0;
				if(velocityx < 0) velocityx = 0;
				x = minx;
			}
			if(x > maxx) {
				if(accelx > 0) accelx = 0;
				if(velocityx > 0) velocityx = 0;
				x = maxx;
			}
			if(y < miny) {
				if(accely < 0) accely = 0;
				if(velocityy < 0) velocityy = 0;
				y = miny;
			}
			if(y > maxy) {
				if(accely > 0) accely = 0;
				if(velocityy > 0) velocityy = 0;
				y = maxy;
			}
		}
	}
}
