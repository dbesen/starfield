package {
	import flash.display.*;
	public class Star extends Sprite {
		public var velocityFactor:Number = 1;
		public function Star(drawColor:uint, radius:Number, velocityFactor:Number) {
			this.velocityFactor = velocityFactor;
			var star:Shape = new Shape();
			star.graphics.beginFill(drawColor);
			star.graphics.lineStyle(0, drawColor);
//			star.graphics.drawCircle(0, 0, radius);
			star.graphics.drawRect(-radius, -radius, radius*2, radius*2); // rectangle is faster
			star.graphics.endFill();
			addChild(star);
		}
		public function move(x:Number, y:Number):void {
			this.x += (x * velocityFactor);
			this.y += (y * velocityFactor);
		}
	}
}
