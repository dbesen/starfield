package {
	import flash.utils.*;
	import flash.events.*;
	import flash.display.*;
	import flash.text.*;
	import flash.ui.*;

	[SWF(backgroundColor="#000000")]
	public class Main extends MovieClip {
		public var starfield:Starfield;
		public var ship:Ship;
		private var elapsed:int = 0;
		private var lastFrameTime:int = 0;
		private static var statusDisplay:TextField = new TextField();
		private var fpsDisplay:TextField = new TextField();

		private var movingRight:Boolean = false;
		private var movingLeft:Boolean = false;
		private var movingUp:Boolean = false;
		private var movingDown:Boolean = false;
		private var firing:Boolean = false;
		private var lastShot:int = 0;
		private var timeBetweenShots:Number = 100;
		public static var gameWidth:Number = 600;
		public static var gameHeight:Number = 400;

		private var enemies:Array = new Array();
		private var shots:Array = new Array(); // player shots
		private var bullets:Array = new Array(); // enemy bullets

		public function Main() {
			stage.scaleMode=StageScaleMode.NO_SCALE;
			stage.align=StageAlign.TOP_LEFT;
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyPress);
			stage.addEventListener(KeyboardEvent.KEY_UP, onKeyRelease);
			starfield = new Starfield();
			addChild(starfield);
			ship = new Ship();
			ship.x = 300;
			ship.y = 300;
			addChild(ship);
			addStatusDisplay();
			startAnimation();
//			addEnemy(300);
			Script.play(this);
		}

		public function addEnemy(x:Number, y:Number):void {
			var enemy:Enemy = new Enemy(this);
			enemy.x = x;
			enemy.y = y;
			addChild(enemy);
			enemies.push(enemy);
		}

		public function onKeyPress(e:KeyboardEvent):void {
			switch(e.keyCode) {
				case Keyboard.RIGHT:
				case 68: // d
					movingRight = true;
					break;
				case Keyboard.LEFT:
				case 65: // a
					movingLeft = true;
					break;
				case Keyboard.UP:
				case 87: // w
					movingUp = true;
					break;
				case Keyboard.DOWN:
				case 83: // s
					movingDown = true;
					break;
				case Keyboard.SPACE:
				case 90: // z
				case 17: // ctrl
					if(firing == false) {
						lastShot = lastFrameTime;
						firing = true;
					}
					break;
			}
		}
		public function onKeyRelease(e:KeyboardEvent):void {
			switch(e.keyCode) {
				case Keyboard.RIGHT:
				case 68: // d
					movingRight = false;
					break;
				case Keyboard.LEFT:
				case 65: // a
					movingLeft = false;
					break;
				case Keyboard.UP:
				case 87: // w
					movingUp = false;
					break;
				case Keyboard.DOWN:
				case 83: // s
					movingDown = false;
					break;
				case Keyboard.SPACE:
				case 90:
				case 17: // ctrl
					firing = false;
					break;
			}
		}
		private function addShot(x:Number, y:Number, elapsed:Number = 0):void {
			// Eash shot is actually two instances of PlayerShot
			var p1:PlayerShot = new PlayerShot(ship, x + 10, y);
			var p2:PlayerShot = new PlayerShot(ship, x - 10, y);
			//var p3:PlayerShot = new PlayerShot(x, y);
			p1.update(elapsed);
			p2.update(elapsed);
			//p3.update(elapsed);
			addChild(p1);
			addChild(p2);
			//addChild(p3);
			shots.push(p1);
			shots.push(p2);
		}
		public function addBullet(bullet:Movable):void {
			bullets.push(bullet);
			addChild(bullet);
		}
		private function addStatusDisplay():void {
			fpsDisplay.x = 520;
			fpsDisplay.y = 0;
			fpsDisplay.textColor = 0x888888;
			addChild(fpsDisplay);
			statusDisplay.x = 0;
			statusDisplay.y = 0;
			statusDisplay.width = 510;
			statusDisplay.textColor = 0xFFFFFF;
			addChild(statusDisplay);
		}
		private function startAnimation():void {
			addEventListener(Event.ENTER_FRAME, onFrame);
/*
			var msPerFrame:Number = 1000 / 60.0; // second number is desired fps
			//msPerFrame = 50; // try it slow
			msPerFrame = 1; // benchmark
			var myTimer:Timer = new Timer(msPerFrame);
			myTimer.addEventListener(TimerEvent.TIMER, onFrame);
			myTimer.start();
*/
		}
//		private function onFrame(e:TimerEvent):void {
		private function onFrame(e:Event):void {
			var currentTime:int = getTimer();
			elapsed = currentTime - lastFrameTime;
			lastFrameTime = currentTime;
			if(elapsed > 33) elapsed = 33;
			var fps:int = 1000 / elapsed;
			fpsDisplay.text = elapsed + "ms (" + fps + " fps)";

			updateGame(currentTime);
		}

		private function updateGame(currentTime:int):void {

//			timeBetweenShots /= 1 + (elapsed / 100000);
			// at 0, time = 100
			// at 50000, time = 5
			timeBetweenShots = 100 - (starfield.getCurrY() / 50000) * 95;
			if(timeBetweenShots < 5) timeBetweenShots = 5;

			updateStarfieldDirection();
			starfield.update(elapsed);
			ship.move(movingLeft, movingRight, movingUp, movingDown, elapsed);
			for(var i:int=0; i<this.numChildren; i++) {
				var item:* = this.getChildAt(i);
				if(item is PlayerShot) {
					var shot:PlayerShot = PlayerShot(item);
					shot.update(elapsed);
					if(shot.y < -10) { // -10 since shots are 10 tall
						removeChildAt(i); i--;
					}
				}
			}
			if(firing) { // go back and fill in shots
				// todo: when the game skips, all the shots' x values are set to the ship's new x, and they should be interpolated
				while(currentTime >= lastShot) {
					addShot(ship.x, ship.y, currentTime - (lastShot));
					lastShot += timeBetweenShots;
				}
			}
			for each (var curr:Enemy in enemies) {
				starfield.alignObject(curr, elapsed);
				curr.update(elapsed);
			}
			for each (var c:Bullet in bullets) {
				starfield.alignObject(c, elapsed);
				c.update(elapsed);
			}
			checkCollisions();
//			if(Math.random() < .01) addEnemy(Math.random() * 500);
			Spawner.update(this);
			//e.updateAfterEvent();
		}
		private function checkCollisions():void {
			for each (var enemy:Enemy in enemies) {
				for each (var shot:PlayerShot in shots) {
					var distX:Number = Math.abs(shot.x - enemy.x);
					var distY:Number = Math.abs(shot.y - enemy.y);
					if(enemy.hitTestObject(shot)) {
						enemy.health -= 1;
						if(enemy.health <= 0)
							enemy.visible = false;
						shot.visible = false;
					}
				}
			}

			var cleanShots:Array = new Array();
			for each (shot in shots) {
				if(shot.visible == true && shot.y > -10)
					cleanShots.push(shot);
//				else // todo: why does this cause no enemies to show up?
//					removeChild(shot);
			}
			shots = cleanShots;

			var cleanEnemies:Array = new Array();
			for each (enemy in enemies) {
				if(enemy.visible == true && enemy.y < gameHeight)
					cleanEnemies.push(enemy);
				else
					removeChild(enemy);
			}
			enemies = cleanEnemies;

			var cleanBullets:Array = new Array();
			for each (var b:Bullet in bullets) {
				if(b.visible == true && b.y < gameHeight && b.y > 0 && b.x > 0 && b.x < gameWidth)
					cleanBullets.push(b);
				else
					removeChild(b);
			}
			bullets = cleanBullets;
		}
		public function getYSpeed():Number {
			var speed:Number = (gameHeight - ship.y) / gameHeight;
			return ((speed * 3) + 1) / 10;
		}
		private function updateStarfieldDirection():void {
			starfield.setVector(-(ship.velocityx / 2), getYSpeed());
		}
		public static function display(stuff:String):void {
			statusDisplay.text = stuff;
		}
	}
}
