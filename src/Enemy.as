package {
	import flash.display.*;
	public class Enemy extends Movable {
		public var health:Number = 10;
		private var shotTime:Number = 0;
		private var timeBetweenShots:Number = 400;
		private var main:Main;
		public function Enemy(main:Main) {
			this.main = main;
			var enemy:Shape = new Shape();
			enemy.graphics.lineStyle(0, 0x333333);
			enemy.graphics.beginFill(0x333333);
			enemy.graphics.drawRect(-25, 0, 50, 30);
			enemy.graphics.endFill();
			addChild(enemy);
			shotTime = Math.random() * timeBetweenShots; // so they're not so uniform
		}
		public override function update(elapsed:Number):void {
			updatePosition(elapsed);
			shotTime += elapsed;

			while(shotTime > timeBetweenShots) {
				addShot();
				shotTime -= timeBetweenShots;
			}

		}
		private function addShot():void {
			var b:Bullet = new Bullet(this);
			b.setTargetMovableLead(main.ship, 0.45, main.getYSpeed());
			main.addBullet(b);
		}
	}
}
