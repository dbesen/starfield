package {
	import flash.display.*;
	public class PlayerShot extends Movable {
		public function PlayerShot(ship:Ship, x:Number, y:Number) {
			this.x = x;
			this.y = y;
//			this.velocityx = ship.velocityx + (Math.random() - .5) * 10;
			this.velocityx = (Math.random() - .5) * 0.2;
			this.velocityy = ship.velocityy + -0.7;
			var shot:Shape = new Shape();
			// todo: tweak the shot color
			shot.graphics.lineStyle(0, 0xFF0000);
			shot.graphics.beginFill(0xFF0000);
			shot.graphics.drawRoundRect(-1, 0, 2, 10, 2);
			shot.graphics.endFill();
			addChild(shot);
		}
	}
}
