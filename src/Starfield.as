package {
	// todo: the density increases as the game goes on
	import flash.display.*;

	public class Starfield extends Sprite {
		private var xVector:Number;
		private var yVector:Number;
		private var density:Number = .0006;
		private var currX:Number = 0;
		private var currY:Number = 0;

		public function Starfield() {
			// Set some sane defaults
			xVector = 0;
			yVector = 1;
			init();
		}
		private function init():void {
			for(var i:int=0;i<Main.gameHeight;i++) {
				update(32);
			}
			currY = 0;
		}
		private function addStars(x0:Number, y0:Number, x1:Number, y1:Number, density:Number):void {
			var nStars:int = numStars(x1-x0, y1-y0, density);
			for(var i:int = 0; i < nStars;i++) {
				addRandomStar(x0, y0, x1, y1);
			}
		}
		// Return the number of stars that should be rendered for a given box and density.
		// This function contains randomality since a density of 3.5 means sometimes 3 and sometimes 4.
		private function numStars(boxWidth:Number, boxHeight:Number, density:Number):int {
			var area:Number = boxWidth*boxHeight; // area in pixels^2
			var nStars:Number = area * density;
			var ret:int = int(nStars);
			if(Math.random() < (nStars - ret)) ret++;
			return ret;
		}
		private function addRandomStar(x0:Number, y0:Number, x1:Number, y1:Number):void {
			var x:Number = Math.random() * (x1-x0) + x0;
			var y:Number = Math.random() * (y1-y0) + y0;
			// The color could be random somehow, but for now it's just white.
			// a radius of 0.125 is not visible, but one of 0.1255 is.
			addStar(x, y, 0xFFFFFF, Math.random()/2 + .5, .874 - Math.sqrt(Math.random()));
		}
		private function addStar(x:Number, y:Number, drawColor:uint, radius:Number, velocityFactor:Number):void {
			radius *= velocityFactor; // The further away it is, the smaller it is.
			if(radius <= 0.125) return;
			var star:Star = new Star(drawColor, radius, velocityFactor);
			star.x = x;
			star.y = y;
			addChild(star);
		}
		public function setVector(x:Number, y:Number):void {
			xVector = x;
			yVector = y;
		}

		public function setVectorNormalized(x:Number, y:Number):void {
			var len:Number = Math.sqrt(Math.pow(x,2) + Math.pow(y,2));
			Main.display("y is " + y + ", x is " + x + ", len is " + len);
			x /= len;
			y /= len;
			xVector = x;
			yVector = y;
		}

		// todo: this code is duplicated
		public function alignObject(enemy:Movable, elapsed:int):void {
			var moveX:Number = (elapsed * xVector);
			var moveY:Number = (elapsed * yVector);
			enemy.x += moveX;
			enemy.y += moveY;
		}

		public function getRelativeX(coord:Number):Number {
			return coord + currX;
		}

		public function getRelativeY(coord:Number):Number {
			return currY - coord;
		}

		public function getCurrY():Number {
			return currY;
		}

		public function update(elapsed:int):void {
			var moveX:Number = (elapsed * xVector);
			var moveY:Number = (elapsed * yVector);
			currX += moveX;
			currY += moveY;
//			Main.display("" + currY);
			for(var i:int=0; i<this.numChildren; i++) {
				var item:Star = Star(this.getChildAt(i));
				item.move(moveX, moveY);
				if(item.x > Main.gameWidth || item.x < 0) { removeChildAt(i); i--; }
				else if(item.y > Main.gameHeight || item.y < 0) { removeChildAt(i); i--; }
			}

			if(moveX >= 0) {
				if(moveY >= 0) {
					//  _
					// |
					addStars(0, 0, Main.gameWidth, moveY, density);
					addStars(0, moveY, moveX, Main.gameHeight, density);
				} else { // moveY is negative
					// |_
					addStars(0, Main.gameHeight + moveY, Main.gameWidth, Main.gameHeight, density);
					addStars(0, 0, moveX, Main.gameHeight + moveY, density);
				}
			} else {
				if(moveY >= 0) { // moveX is negative
					// _
					//  |
					addStars(0, 0, Main.gameWidth, moveY, density);
					addStars(Main.gameWidth + moveX, moveY, Main.gameWidth, Main.gameHeight, density);
				} else { // both are negative
					// _|
					addStars(0, Main.gameHeight + moveY, Main.gameWidth, Main.gameHeight, density);
					addStars(Main.gameWidth + moveX, 0, Main.gameWidth, Main.gameHeight + moveY, density);
				}
			}
		}
	}
}
