package {
	import flash.display.*;
	public class Ship extends Movable {
		public function Ship() {
			var ship:Shape = new Shape();
			ship.graphics.lineStyle(0, 0x000000);
			ship.graphics.moveTo(0, -20);
			ship.graphics.beginFill(0x666666);
			ship.graphics.lineTo(15, 20);
			ship.graphics.lineTo(-15, 20);
			ship.graphics.lineTo(0, -20);
			ship.graphics.endFill();
			addChild(ship);

			maxvelocityx = 0.3;
			maxvelocityy = 0.3;
			deceleration = 0.01;
			minx = 15;
			maxx = Main.gameWidth - 15;
			miny = 35;
			maxy = Main.gameHeight - 20;
		}
		public function move(movingLeft:Boolean, movingRight:Boolean, movingUp:Boolean, movingDown:Boolean, elapsed:Number):void {
			accelx = 0;
			accely = 0;
			if(movingUp && !movingDown) {
				accely = -0.2;
			}
			if(movingDown && !movingUp) {
				accely = 0.2;
			}
			if(movingLeft && !movingRight) {
				accelx = -0.2;
			}
			if(movingRight && !movingLeft) {
				accelx = 0.2;
			}
			update(elapsed);
		}
	}
}
