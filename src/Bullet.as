package {
	import flash.display.*;
	public class Bullet extends Movable {
		// todo: homing bullets
/*
		public function Bullet(source:Movable, target:Movable, speed:Number) {
			this(source, (target.x - source.x), (target.y - source.y), speed);
		}
*/

		private var source:Movable;
		// This only works if the ship is faster than the bullet.
		public function setTargetMovableLead(target:Movable, bulletSpeed:Number, targetYSpeed:Number):void {
			// todo: this is pretty expensive, is there a cheaper way?
			var len:Number = Math.sqrt(Math.pow(target.x - source.x,2) + Math.pow(target.y-source.y,2));
			var angle:Number = Math.atan2(target.x - source.x, target.y - source.y);
			var sinangle2:Number = Math.sin(angle) * (targetYSpeed / bulletSpeed);
			var angle2:Number = Math.asin(sinangle2);
			var angle3:Number = Math.PI - angle - angle2;
			var yoffset:Number = len * (sinangle2 / Math.sin(angle3));
			setTargetVector(target.x - source.x, target.y - source.y - yoffset, bulletSpeed);
		}

		public function setTargetMovable(target:Movable, speed:Number):void {
			setTargetVector((target.x - source.x), (target.y - source.y), speed);
		}

		public function setTargetVector(vectorX:Number, vectorY:Number, speed:Number):void {
			var len:Number = Math.sqrt(Math.pow(vectorX,2) + Math.pow(vectorY,2));
			vectorX /= len;
			vectorY /= len;
			velocityx = vectorX * speed;
			velocityy = vectorY * speed;
		}

		public function Bullet(source:Movable) {
			this.source = source;
			render();
			x = source.x;
			y = source.y;
		}

		public function render():void {
			var b:Shape = new Shape();
			b.graphics.lineStyle(2, 0x0000FF);
			b.graphics.beginFill(0x00FFFF);
			b.graphics.drawCircle(0, 0, 5);
			b.graphics.endFill();
			addChild(b);
		}
	}
}
	
