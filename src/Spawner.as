package {
	public class Spawner {
		private var starfieldX:Number;
		public var starfieldY:Number;
		public function Spawner(starfieldX:Number, starfieldY:Number) {
			this.starfieldX = starfieldX;
			this.starfieldY = starfieldY;
		}
		public function execute(main:Main):void {
			var x:Number = main.starfield.getRelativeX(starfieldX);
			var y:Number = main.starfield.getRelativeY(starfieldY);
			main.addEnemy(x, y - 100);
		}
		public function toString():String {
			return "" + starfieldY + ", " + starfieldX;
		}

		public static var upcoming:Array = new Array();
		public static function at(x:Number, y:Number):void {
			upcoming.push(new Spawner(x, y));
			upcoming.sortOn("starfieldY", Array.DESCENDING | Array.NUMERIC);
		}

		public static function update(main:Main):void {
			var currY:Number = main.starfield.getCurrY();
			while(upcoming.length > 0 && upcoming[upcoming.length-1].starfieldY <= currY) {
				upcoming.pop().execute(main);
			}
		}
	}
}
